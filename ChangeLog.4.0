
=== 2340compressSources-bf.cs ===
Change Set:		compressSources-bf
Date:			14 January 2010
Author:			Bert Freudenberg

Fix compressing sources to preserve utf8 encoding. Fix reading chunks to decode utf8.
Decode chunk language tags, too.
TODO: figure out if anything else than chunk reading is used. decode utf8 there, too.

=== 2339viewSourceDBus-bf.cs ===
Change Set:		viewSourceDBus-bf
Date:			25 November 2009
Author:			Bert Freudenberg

Handle Sugar's view-source DBus message

=== 2338otherActivities-bf.cs ===
Change Set:		otherActivities-bf
Date:			25 November 2009
Author:			Bert Freudenberg

Prevent automatic saving of projects of non-Etoys activities. Also, when resuming such an activity with an explicitly saved project, do not load both from the bundle and from the journal, but just the latter.

=== 2337smarterSetupVirtDisp-yo.cs ===
Change Set:		smarterSetupVirtDisp-yo
Date:			20 November 2009
Author:			Yoshiki Ohshima

Disable virtual display when running in Sugar at 1200x900 pixels.
Postscript checks this immediately.

=== 2336ftpClient-kzy.cs ===
Change Set:		ftpClient-kzy
Date:			17 October 2009
Author:			Koji Yokokawa

Make FTPClient work again

=== 2335JapaneseMacVM4-mu.cs ===
Change Set:		JapaneseMacVM4-mu
Date:			1 July 2009
Author:			Masashi Umezawa

Added a support of input and clipboard copy & paste in Japanese especially for Mac VM 4.*.

=== 2334storeOnServerFix-mu.cs ===
Change Set:		storeOnServerFix-mu
Date:			11 October 2009
Author:			Masashi Umezawa

Project>>storeOnServerShowProgressOn:forgetURL: will raise DNU because MessageSend argument is not set properly.
The patch adds projectDetails argument.  



=== 2333unknownFix-sw.cs ===
Change Set:		unknownFix-sw
Date:			12 October 2009
Author:			Scott Wallace

Fix for SQ-510:  strange URI for Fraction Bars 31.

Correct the uri transformation made when a project previously saved with 'unknown' user gets re-saved on behalf of a registered user.

=== 2332trnSlotType-KR.cs ===
Change Set:		trnSlotType-KR
Date:			6 October 2009
Author:			Korakurider

SQ-483:  name of slot type not extracted to POT ans can't be translated

=== 2331trnFuncTile-KR.cs ===
Change Set:		trnFuncTile-KR
Date:			3 October 2009
Author:			Korakurider

SQ-485:  to correctly translate function tiles

=== 2330funcTileForm-KR.cs ===
Change Set:		funcTileForm-KR
Date:			3 October 2009
Author:			Korakurider

SQ-485  build graphical icon for random' and 'abs' in gold box menu dynamically 

=== 2329suffixArrowTweak-sw.cs ===
Change Set:		suffixArrowTweak-sw
Date:			4 October 2009
Author:			Scott Wallace

Fix for the follow-on issue in the reopened version of SQ-450 missing expand-arrows.

Now deals with the special case of a bearing-to or distance-to tile being dropped into a TEST pane.

=== 2328etoyCodeFont-Richo.cs ===
Change Set:		etoyCodeFont-Richo
Date:			30 September 2009
Author:			Ricardo Moran

This change set changes the default etoy code font to size 15.
Postscript restores the default fonts to set the new size

=== 2327useSymbolKey-sw.cs ===
Change Set:		useSymbolKey-sw
Date:			3 October 2009
Author:			Scott Wallace

Addresses SQ-488 'Prev-URI not set in manifest'.  Use symbol keys for the project-info items stored in project parameters.  Thanks to Yoshiki for pointing out the root of the problem.

=== 2326pasteFixUnix-bf.cs ===
Change Set:		pasteFixUnix-bf
Date:			30 September 2009
Author:			Bert Freudenberg

Fix clipboard on Unix (introduced in 2324pasteFix-bf.cs)

=== 2325reduceBlockUsage-sw.cs ===
Change Set:		reduceBlockUsage-sw
Date:			28 September 2009
Author:			Scott Wallace

Cease using blocks in the actionBlock mechanism in the EToyProjectDetailsMorph and related classes.   This turned out to involve a surprising number of methods.

Some of the code affected is not reachable in the current image so may not have been deeply tested; the EToyProjectRenamerMorph is never instantiated these days (presumably it once was,) and the EToyProjectQueryMorph is only referenced by a method which itself is never called.  But I converted all the  code that relies on the actionBlock being a block anyway, since possibly there are packages (e.g. superswiki implementations) which may need them in their de-blockified form.

=== 2324pasteFix-bf.cs ===
Change Set:		pasteFix-bf
Date:			29 September 2009
Author:			Bert Freudenberg

Move ExtendedClipboard text handling to Clipboard class, to retain text formatting when copied from Etoys.

=== 2323ISO639Update-yo.cs ===
Change Set:		ISO639Update-yo
Date:			28 September 2009
Author:			Yoshiki Ohshima

Update the in image ISO 639 language code table.  The country code is not updated by this changeset.

=== 2322deferEvents-bf.cs ===
Change Set:		deferEvents-bf
Date:			29 September 2009
Author:			Bert Freudenberg

Defer event handling to after startup. This is because when launching Etoys with a project on the Mac, the VM creates a drop event, entering which terminates the startup sequence prematurely.

=== 2321suffixArrowOnFn-sw.cs ===
Change Set:		suffixArrowOnFn-sw
Date:			28 September 2009
Author:			Scott Wallace

Addresses SQ-450 - 'missing expand-arrow'.

Supply parentheses around 'bearing to' and 'distance to' tiles when they are dropped  into a script, so that the resulting numeric entities will have  extension arrows, allowing further arithmetic with the result of the function computations.

=== 2320lockNavBar-kks.cs ===
Change Set:		lockNavBar-kks
Date:			24 September 2009
Author:			kks

Lock the Sugar Navigation Bar against accidental deletion

=== 2319LoginNetErroGuard2-yo.cs ===

=== 2318cacheClean-bf.cs ===
Change Set:		cacheClean-bf
Date:			17 September 2009
Author:			Bert Freudenberg

Make sure to truncate the cached files before overwriting.

=== 2317africa-sw.cs ===
Change Set:		africa-sw
Date:			17 September 2009
Author:			Scott Wallace

Africa had been missing from the default region list.  Also, renumbering the region codes.

=== 2316ShowcaseURL-yo.cs ===
Change Set:		ShowcaseURL-yo
Date:			17 September 2009
Author:			Yoshiki Ohshima

Further fix for 2314 to make it work with http://content.squeakland.org/showcase.

=== 2315LoginNetErrorGuard-yo.cs ===
Change Set:		LoginNetworkErrorGuard-yo
Date:			16 September 2009
Author:			Yoshiki Ohshima

More kinds of errors may happen.  Guard against them.

=== 2314ShowcaseURL-yo.cs ===
Change Set:		ShowcaseURL-yo
Date:			16 September 2009
Author:			Yoshiki Ohshima

SQ-389, if the path is http://content.squeakland.org/showcase/

=== 2313cacheCats-bf.cs ===
Change Set:		cacheCats-bf
Date:			16 September 2009
Author:			Bert Freudenberg

Cache squeakland categories on file. Update after logging in to showcase

=== 2312assureLastModified-yo.cs ===
Change Set:		assureLastModified-yo
Date:			16 September 2009
Author:			Yoshiki Ohshima

getlastmodified may be empty or malformed.

=== 2311notLoggedInTR-yo.cs ===

=== 2310SignupPanel-yo.cs ===
Change Set:		SignupPanel-yo
Date:			15 September 2009
Author:			Yoshiki Ohshima

SQ-382.

=== 2309grabbersInSupplies-sw.cs ===
Change Set:		grabbersInSupplies-sw
Date:			3 September 2009
Author:			Scott Wallace

SQ-267 Lasso and Grab patch tools back to Supplies flap.

=== 2308GrabLassoCursor-tk.cs ===
Change Set:		GrabLassoCursor-tk
Date:			15 September 2009
Author:			Ted Kaehler

SQ-328 Show a cursor throughout patch-grabbing.  This update fixes the remaining cursor issues.

=== 2307fixBalloonHelp-bf.cs ===
Change Set:		fixBalloonHelp-bf
Date:			15 September 2009
Author:			Bert Freudenberg

Replace non-undo-related uses of undoButton with paintButton.
NOTE: Changeset has been modified manually.
Postscript rebuilds tool bar

=== 2306bulletproofCats-sw.cs ===
Change Set:		bulletproofCats-sw
Date:			14 September 2009
Author:			Scott Wallace

Provide some bulletproofing against the possibility that the project-category information obtained from the squeakland web site might be corrupt.

=== 2305FontAvailCheck-yo.cs ===
Change Set:		FontAvailCheck-yo
Date:			31 August 2009
Author:			Yoshiki Ohshima

Mitigate the font not available problem.

=== 2304askProjectInfo-yo.cs ===
Change Set:		askProjectInfo-yo
Date:			14 September 2009
Author:			Yoshiki Ohshima

a possible patch to implement the suggested behavior at SQ-379.

=== 2303sugarObjectId-bf.cs ===
Change Set:		sugarObjectId-bf
Date:			14 September 2009
Author:			Bert Freudenberg

Remember Sugar object id in each project rather than globally. This prevents accidental overwriting of project in the Journal

=== 2302noUndoButton-sw.cs ===
Change Set:		noUndoButton-sw
Date:			13 September 2009
Author:			Scott Wallace

SQ-378 -- remove the undo button from the navigator bar.  This change-set removes the undo item from all the variants of project-navigator-bar.
bf: Postscript rebuilds nav bar.

=== 2301loadSaveIcons-bf.cs ===
Change Set:		loadSaveIcons-bf
Date:			12 September 2009
Author:			Bert Freudenberg

If running outside Sugar, use folder-like icons for load and save. In Sugar, replace load icon with the Sugar-standard insert-object icon. To test:
SugarLauncher current parameters at: 'ACTIVITY_ID' put: '123'
SugarLauncher current parameters removeKey: 'ACTIVITY_ID'


=== 2300quitOrStop-bf.cs ===
Change Set:		quitOrStop-bf
Date:			11 September 2009
Author:			Bert Freudenberg

Make Quit button look like a quit button (under Sugar it still is the Stop sign).

=== 2299manifestAdditions-sw.cs ===
Change Set:		manifestAdditions-sw
Date:			11 September 2009
Author:			Scott Wallace

Adds items to the project manifest:
  user  		The squeakland user-name of the user publishing the project,
				if the user is logged in; if not logged in, this will be 'unknown'.
  URI 		A URI of the form http://squeakland.org/etoys/<user>-<secondsSinceEpoch>
  prev-URI 	The prior URI for this project, if user has changed 

=== 2298renameWelcome-bf.cs ===
Change Set:		renameWelcome-bf
Date:			11 September 2009
Author:			Bert Freudenberg

Rename the welcome project from 'Etoys Activity' to 'Home'.

=== 2297ProjectDAV5-yo.cs ===
Change Set:		ProjectDAV5-yo
Date:			11 September 2009
Author:			Yoshiki Ohshima

Change the layout of login box and put default label when not logged in. Also the default path name is changed to everyone from public.

=== 2296ProjectDAV4-yo.cs ===
Change Set:		ProjectDAV4-yo
Date:			11 September 2009
Author:			Yoshiki Ohshima

Hook up with primOpenURL:.  Add #translated a few more places.

=== 2295authorName-sw.cs ===
Change Set:		authorName-sw
Date:			11 September 2009
Author:			Scott Wallace

Bulletproof Utilities authorName against nil value for the name -- needed after some recent change.

=== 2294AdjustNameField-yo.cs ===
Change Set:		AdjustNameField-yo
Date:			11 September 2009
Author:			Yoshiki Ohshima

Bullet proof against various name field size change.

=== 2293savebuttonhelp-yo.cs ===
Change Set:		savebuttonhelp-yo
Date:			9 September 2009
Author:			Yoshiki Ohshima

Now the stuff is above the button but comment said otherwise.

=== 2292BalloonHelpForLogin-yo.cs ===
Change Set:		BalloonHelpForLogin-yo
Date:			11 September 2009
Author:			Yoshiki Ohshima

Adds help to new button and field.

=== 2291ScratchPlugin-jm.cs ===
Change Set:		ScratchPlugin-jm
Date:			11 September 2009
Author:			John Maloney (packaged by Bert Freudenberg)

This is the (class-side of the) ScratchPlugin class which provides primitive accessors to the ScratchPlugin. Reused with permission from the ScratchCode1.3.1 release. Thanks to Mitchel and John and the other Lifelong Kindergardeners

=== 2290prevProject-bf.cs ===
Change Set:		prevProject-bf
Date:			11 September 2009
Author:			Bert Freudenberg

Simplify prevButton logic: if there is no proper previous project, set the current project's parent  to the welcome screen (cloudy launcher), and exit to that.

=== 2289toolBarHelpTweak-sw.cs ===
Change Set:		toolBarHelpTweak-sw
Date:			10 September 2009
Author:			Scott Wallace

SQ-257 When auto-hiding toolbar (when opening an 'old' project created in a version of the system before we had the tool bar at the top), initially show the balloon help for the show-tool-bar button

=== 2288HierListAutoExpand-yo.cs ===
Change Set:		HierListAutomaticallyExpand-yo
Date:			11 September 2009
Author:			Yoshiki Ohshima

When the name part of indented list is clicked the content is automatically expand.  (not just the small triangle).

Now each hierarchical morph and list items have #autoExpand property.  When it is true (per item basis), it automatically expands.  In this change set, only the use from FileList2 is taking advantage of it, but if it is found useful the other parts (like explorer) and do the same.

=== 2287sugarScale2-bf.cs ===
Change Set:		sugarScale2-bf
Date:			11 September 2009
Author:			Bert Freudenberg

Forgot to enable screen scaling

=== 2286sugarScale-bf.cs ===
Change Set:		sugarScale-bf
Date:			11 September 2009
Author:			Bert Freudenberg

When building sugar image, make it 800x600 by default

=== 2285SaveLoadAutoSelect-yo.cs ===
Change Set:		SaveLoadAutoSelect-yo
Date:			9 September 2009
Author:			Yoshiki Ohshima

Automatically selects the local document dir by default.

=== 2284ProjectDAV3-yo.cs ===
Change Set:		ProjectDAV3-yo
Date:			9 September 2009
Author:			Yoshiki Ohshima

Better error recovery.  The save and load dialogs now have login button and current user's name.  The apperance is slightly modified based on SQ-269.

=== 2283noReadingAStream-sw.cs ===
Change Set:		noReadingAStream-sw
Date:			3 September 2009, 7 September 2009
Author:			Scott Wallace

Fix for SQ-237  Gratuitous 'Reading a stream' message.
Version of 7 September follows Bert's advice on JIRA:
(a) Don't use the 'format:' construct for the message after all.
(b) Run the stream-name through 'translated' before deciding whether or not we're in the degenerate case.

As Bert suggests on JIRA, perhaps we want to suppress the progress informer in general -- if we decide to adopt that suggestion, the code in this fileout would be unnecessary.

=== 2282BalloonsForLocalDir-yo.cs ===
Change Set:		BalloonsForLocalDir-yo
Date:			9 September 2009
Author:			Yoshiki Ohshima

Add balloon helps for local dirs to show the full path.

=== 2281projInfoRearrange-sw.cs ===
Change Set:		projInfoRearrange-sw
Date:			10 September 2009
Author:			Scott Wallace

Rearrange items in project-info dialog, and restore (none) as the default wording for pop-up items for which no value has been specified.

=== 2280ProjectDAV2-yo.cs ===
Change Set:		ProjectDAV2-yo
Date:			8 September 2009
Author:			Yoshiki Ohshima

Remember a password forever.

=== 2279localizedCats-sw.cs ===
Change Set:		localizedCats-sw
Date:			8 September 2009
Author:			Scott Wallace

Obtain language-specific category-lists from the web site where possible.

=== 2278navbarButtons-bf.cs ===
Change Set:		navbarButtons-bf
Date:			8 September 2009
Author:			Bert Freudenberg

Adjust navbar buttons: add Next button, hide Fullscreen button, show Share button. Outside of Sugar always show Fullscreen button but hide Share button.

=== 2277grabPatchFeedback-sw.cs ===
Change Set:		grabPatchFeedback-sw
Date:			6 September 2009
Author:			Scott Wallace

Show crosshair feedback during patch-grabbing.
Restore display after patch-grabbing, to correct for gribblies sometimes left behind.

=== 2276displayModeMenu-sw.cs ===
Change Set:		displayModeMenu-sw
Date:			8 September 2009
Author:			Scott Wallace

Add display-mode item to the world's halo menu.

=== 2275sugarToolbar-bf.cs ===
Change Set:		sugarToolbar-bf
Date:			8 September 2009
Author:			Bert Freudenberg

Adjust navbar under Sugar: hide Fullscreen button, add Next button. Show Share button only in Sugar.

=== 2274noGifThumbnail-sw.cs ===
Change Set:		noGifThumbnail-sw
Date:			1 September 2009
Author:			Scott Wallace

No longer write out the gif thumbnail.
No longer include the filename of the gif thumbnail in info transmitted to the SuperSwikiServer.
Consequences for existing superSwikiServers and other project servers unknown -- need help testing. Project servers are now expected to cut over to using the .png thumbnail embededded in the outer .pr file.

=== 2273SSUploadProjEncFix-mu.cs ===
Change Set:		SuperSwikiUploadProjectEncodingFix-mu
Date:			8 September 2009
Author:			Masashi Umezawa
SuperSwikiServer's default encoding should be always utf-8.

=== 2272projInfoFix-sw.cs ===
Change Set:		projInfoFix-sw
Date:			8 September 2009
Author:			Scott Wallace

Fixes an integration issue between updates 2268 and 2269.

=== 2271codeFont-bf.cs ===
Change Set:		codeFont-bf
Date:			8 September 2009
Author:			Bert Freudenberg

- fix default code font size

=== 2270limitedPaint-bf.cs ===
Change Set:		limitedPaint-bf
Date:			8 September 2009
Author:			Bert Freudenberg

Disable unlimitedPaintArea

=== 2269projectInfoPopUps-sw.cs ===
Change Set:		projectInfoPopUps-sw
Date:			24 August 2009
Author:			Scott Wallace

Changes to the project-info dialog in support of the 'showcase'.

-  'Sub-Category' removed.
-  'Category' appears as 'Subject'.
-  Subject, Target Age, and Region are added as pop-ups.
-  Choices for Subject, Target Age, and Region, and corresponding codes, obtained from web-site where possible.
-  Project manifest now includes Age, and Region info.  Subject is represented as before as projectcategory. The codes are strings of digits, e.g. '554' as a Subject code means 'Language Arts'.
-  Names of fields are presented to the user in localized form.
-  Values of pop-up fields are presented to the user in localized form.

=== 2268ProjectDAV-yo.cs ===
Change Set:		ProjectDAV-yo
Date:			8 September 2009
Author:			Yoshiki Ohshima

A stub for adding webdav in the dialog.

=== 2267sugarDirEntries-bf.cs ===
Change Set:		sugarDirEntries-bf
Date:			8 September 2009
Author:			Bert Freudenberg

Handle ByteArrays we get from the Datastore instead of Strings. This was breaking saving under recent Sugar, wonder why nobody noticed?

=== 2266swapCtrlAlt-bf.cs ===
Change Set:		swapCtrlAlt-bf
Date:			7 September 2009
Author:			Bert Freudenberg

Restore swapping on Linux.

=== 2265PinMorphEmbedding-kfr.cs ===
Change Set:		PinMorphEmbedding-kfr
Date:			7 June 2009
Author:			Karl Ramberg/Bert Freudenberg

PinMorph popped up a menu about embedding in objects tool. I added a check for where it is embedded in quite a ugly way but it made the pop  up menu go away...
Edit: use a slightly better workaround --bf

=== 2264clearHandUponLoading-yo.cs ===
Change Set:		clearHandUponLoading-yo
Date:			7 September 2009
Author:			Yoshiki Ohshima

Reset keyboard interpreter upon loading.

=== 2263downloadLink-sw.cs ===
Change Set:		downloadLink-sw
Date:			17 July 2009
Author:			Scott Wallace

SQ-270:  change .pr download link.

This update changes the url in the download link as specified in JIRA ticket SQ-270

=== 2262sugarSaveHelp-sw.cs ===
Change Set:		sugarSaveHelp-sw
Date:			3 September 2009
Author:			Scott Wallace

Fix for SQ-294 Save Project balloon help wrong under Sugar.

=== 2261lookLikeBug-sw.cs ===
Change Set:		lookLikeBug-sw
Date:			2 September 2009
Author:			Scott Wallace

Avoid the hopeless special-purpose spaghetti that doomed the wear-costume code to numerous problems.  Instead, make look-like now directly call the graphic-setting code, and make look-like only available for SketchMorphs.  Among other things, this clears up 'Alex's bug' in which tearing off a sibling of a morph that has a ticking script which calls 'look like' generated an error.

Version of 2Sept -- place a neutral offset into the form being applied as the new costume, thus avoiding the offset bug reported in http://jira.immuexa.com/browse/SQ-316.

2 Sept second version -- copy over center of rotation, etc.. when appropriate.

=== 2260ctrlAltDefault-bf.cs ===
Change Set:		ctrlAltDefault-bf
Date:			31 August 2009
Author:			Bert Freudenberg

Enable #swapControlAndAltKeys on non-Mac platforms

=== 2259swapCtrlAltAt6-yo.cs ===
Change Set:		swapCtrlAltAt6-yo
Date:			31 August 2009
Author:			Yoshiki Ohshima

The sixth entry should be changed, too.

=== 2258cloudsFix-sw.cs ===
Change Set:		cloudsFix-sw
Date:			27 August 2009
Author:			Scott Wallace

Reconsiders the logic introduced in 2250backToClouds-sw.

=== 2257VanishingWithScaling-yo.cs ===
Change Set:		VanishingWithScaling-yo
Date:			24 August 2009
Author:			Yoshiki Ohshima

When old sliding logic is used, the scaled display update can be messed up.  Just inserting the forceToScreen at the last part fixes it.

=== 2256KedamaFixesAug24-yo.cs ===
Change Set:		KedamaFixesAug24-yo
Date:			24 August 2009
Author:			Yoshiki Ohshima

1. when you open debugger or inspector on a kedama player, evaluation of an expression fails as there may not be a costume.  2. turtle count is not defined for the vector object.

=== 2255viewerSpanFix-rm.cs ===
Change Set:		viewerSpanFix-rm
Date:			24 August 2009
Author:			Ricardo Moran; published by Scott Wallace

Fix for a serious performance bug introduced in update 2234viewerBeneath-sw

=== 2254hostWindowSize-bf.cs ===
Change Set:		hostWindowSize-bf
Date:			22 August 2009
Author:			Bert Freudenberg

Add DisplayScreen class>>hostWindowSize: to change the window size

=== 2253PianoKeyboardMorph-kfr.cs ===
Change Set:		PianoKeyboardMorph
Date:			15 March 2009
Author:			Karl Ramberg

Makes the piano keyboard about twice as big

=== 2252duplicateViewerFix-sw.cs ===
Change Set:		duplicateViewerFix-sw
Date:			13 August 2009
Author:			Scott Wallace

Fix for JIRA ticket 292.  In some circumstances, when a viewer is opened on an object in a project loaded from disk, two copies of the viewer's contents appear, stacked one upon the other.

=== 2251fixCursor-bf.cs ===
Change Set:		fixCursor-bf
Date:			17 August 2009
Author:			Bert Freudenberg

On the Mac, the cursor looks jagged. Its alpha was not pre-multiplied. Also, it had a bluish tint. Postscript initializes new cursor.

=== 2250backToClouds-sw.cs ===
Change Set:		backToClouds-sw
Date:			13 July 2009
Author:			Scott Wallace

Wire up the prev-project button to the 'clouds' project if there is no previous project. (Edited slightly by bf)

=== 2249manifestsFix-bf.cs ===
Change Set:		manifestsFix-bf
Date:			18 August 2009
Author:			Bert Freudenberg

Fixes a bug seen when attempting to load a project which lacks a manifest.

(Published by Scott Wallace, based on fix posted by Bert to etoys-dev on 14 aug 2009.)

=== 2248collapseTweak-sw.cs ===
Change Set:		collapseTweak-sw
Date:			18 August 2009
Author:			Scott Wallace

Use a more felicitous translation idiom in a recently-modified method.

=== 2247LangInManifest-yo.cs ===
Change Set:		LangInManifest-yo
Date:			6 August 2009
Author:			Yoshiki Ohshima

Add a new key in the manifest file to not the language information.

=== 2246PolygonFix-kfr.cs ===
Change Set:		PolygonFix-kfr
Date:			3 May 2009
Author:			Karl Ramberg, Tetsuya Hayashi

Hayashi-san's February 2009 fixes for SQ-163 PolygonMorph issues, together with Karl's May 2009 tweak to StarMorph.

=== 2245thumbnailInsideZip-sw.cs ===
Change Set:		thumbnailInsideZip-sw
Date:			18 July 2009
Author:			Scott Wallace

Store a png with the project's thumbnnail as one of the members comprising the outer zip that is the pr file... SQ-274.

Version 2:  use the fixed name, thumbnail.png, rather than naming the thumbnail after the project itself, as per Bert's suggestion.

=== 2244sketchThumbLoc-sw.cs ===
Change Set:		sketchThumbLoc-sw
Date:			11 June 2009
Author:			Scott Wallace

Version 2:  6 August 2009 -- incorporates Hilaire's suggestion to make the help-message for the collapsed thumbnail be translatable.

A simple solution to the issue raised in JIRA SQ-252 'The collapsed sketch should appear below the toolbar, not on top of it.

=== 2243saveProjectButton-sw.cs ===
Change Set:		saveProjectButton-sw
Date:			18 July 2009
Author:			Scott Wallace
Version 2 - 6 August 2009, incorporates Hilaire's recommendations.

Improves and harmonizes the help-messages and menu wordings involved with the side-by-side 'load project' and 'save project' buttons in the nav-bar.

Also changes 'Key Words' to 'Tags' in the project info dialog.

Also Changes 'Name' to 'Project Name' in the project info dialog.

=== 2242revealAndGrab-sw.cs ===
Change Set:		revealAndGrab-sw
Date:			4 August 2009
Author:			Scott Wallace

6 August 2009:  make the wording of the hand-me-a-tile item in the halo-actions submenu consistent with the below, as recommended by Antonio.  The two other balloon-help situations mentioned by Antonio remain somewhat inconsistent with other balloon-help wordings for hand-me-a-tile, but are retained in their current form to maintain consistency with balloon-help wordings for adjacent items in the same tool.

Harmonizes menus that have 'reveal object', 'grab object', and 'tile for object' items, so that they all offer all three items, with the same wording and compatible balloon-help.

This finally catches up with a  very reasonable recommendation from Kathleen Harness long ago.

=== 2241macClipFix-bf.cs ===
Change Set:		macClipFix-bf
Date:			17 July 2009
Author:			Ted Kaehler/Bert Freudenberg

This is derived from Ted's unicodeMacExternal changeset:
TK: After the latest Unicode cut and paste changes, text copied from Eudora on a Mac could not be pasted.  I revivied some old code for when the latest conversion fails.  Now both unicode and older Mac text from outside Squeak can be pasted.
BF: Convert from MacRoman charset to Unicode.
BF: ... and make sure we answer a Text not String.
BF: ... and convert LFs to CRs.
BF: ... and refactor the whole so it makes sense and is less verbose

=== 2240lexiFix-sw.cs ===
Change Set:		lexiFix-sw
Date:			4 August 2009
Author:			Scott Wallace

For some reason, in the etoys image, the lexicon and instance-browser tools have been opening with traces of the old candy-colored early squeak look, even though the alternativeWindowLook preference is set.  This update provides a workaround that assures that these tools open with the proper look.

=== 2239grabPatch-kfr.cs ===
Change Set:		grabPatch-kfr
Date:			3 August 2009
Author:			Karl Ramberg (& Scott Wallace)

Improved grab-patch tool -- see SQ-99 on tracker.squeakland.org.  Allows grab action to proceed in any direction.

=== 2238fullScreenCheckbox-sw.cs ===
Change Set:		fullScreenCheckbox-sw
Date:			5 August 2009
Author:			Scott Wallace

Makes the full-screen/non-full-screen control in the scaling menu be a checkbox, just like all the other toggles in that menu.
Make a click on the checkbox representing the current screen-mode deslelect and select another screen mode in its place.

=== 2237sqLandConfigJul17-yo.cs ===
Change Set:		sqLandConfigJul17-yo
Date:			17 July 2009
Author:			Yoshiki Ohshima

Set to use Bigger cursor.  Make font bigger.

=== 2236navBar-sw-mz-yo.cs ===
Change Set:		navBar-sw-mz-yo
Date:			16 July 2009
Author:			Scott Wallace, Milan Zimmermann, Yoshiki Oshima

Adds a control at topright of screen/window to govern visibility of the full nav bar.

When opening 'old' projects, the nav bar will now appear in its collapsed form, at the top-right corner of the screen, to minimize the chance of obscuring essential content.  The criterion for oldness is:  any project lacking a 'manifest' is considered old, as is any project whose manifest indicates a version code that is not 'etoys'.

This update is derived from work done under the following two JIRA items:
	 SQ-154    	 Make it simple to show and hide the toolbar
	 SQ-280    	 Detect the version of a loading project and hide nav bar if desirable.

=== 2235enterNew-sw.cs ===
Change Set:		enterNew-sw
Date:			11 June 2009
Author:			Scott Wallace

JIRA SQ-257 (Tim) Should the 'new project'  button open the newly created project, like the 'Make a project' cloud?

  The difference in user experience is disturbing, and it might not be obvious to new users that they need to click the little project window.

=== 2234viewerBeneath-sw.cs ===
Change Set:		viewerBeneath-sw
Date:			6 July 2009
Author:			Scott Wallace

Fix for JIRA issue SQ-88:  Position the viewer beneath the nav-bar,

=== 2233toggleFullScreen-bf.cs ===
Change Set:		toggleFullScreen-bf
Date:			7 July 2009
Author:			Bert Freudenberg

Make the zoom button toggle full screen mode, hold down to show display options.

=== 2232ChicagoTheme-yo.cs ===
Change Set:		ChicagoTheme-yo
Date:			13 July 2009
Author:			Yoshiki Ohshima

Chicago theme redone. Not to use green for the bar.

=== 2231strtUpPjEncoding-KR.cs ===
Change Set:		strtUpPjEncoding-KR
Date:			23 June 2009
Author:			Korakurider

SQ-265: Startup project name has to be decoded as VM-neutral encoding, not systemString.


=== 2230SqueaklandRelease09-yo.cs ===
Change Set:		SqueaklandRelease09-yo
Date:			9 July 2009
Author:			Yoshiki Ohshima

Adopt the new policy on the screen size, where almost always use the virtual screen.

=== 2229hideHaloFix-kfr.cs ===
Change Set:		hideHaloFix-kfr
Date:			6 June 2009
Author:			Karl Ramberg

A dropped SelectionMorph still showed its halo, which could give nasty errors if a handle was clicked

=== 2228allKeystrokes-sw.cs ===
Change Set:		allKeystrokes-sw
Date:			7 May 2009
Author:			Scott Wallace

Report all keystrokes to all consumers of  #lastKeystroke, even if they are also consumed by some object other than the World.

=== 2227stepIntoFix-bf.cs ===
Change Set:		stepIntoFix-bf
Date:			22 May 2009
Author:			Bert Freudenberg

The debugger did not step into blocks evaluated using #valueWithArguments:. This uses primitive 82 which was not properly special-cased yet in #doPrimitive:method:receiver:args: so  the debugger did not regain control until the block was done. This changeset handles this case.

=== 2226projectTitle-bf.cs ===
Change Set:		projectTitle-bf
Date:			30 April 2009
Author:			Bert Freudenberg

Set main window title to project name (if projectNameInTitle pref is true)

=== 2225setTitle-bf.cs ===
Change Set:		setTitle-bf
Date:			29 April 2009
Author:			Bert Freudenberg

Add a method to set the main window's title using the HostWindowPlugin

=== 2224fillBlank-sw.cs ===
Change Set:		fillBlank-sw
Date:			27 May 2009
Author:			Scott Wallace

Adds a #layoutChanged call in a FillInTheBlank method... which is hard to justify except that it does happen to eliminate the bug symptom reporter in the Squeakland Tracker as issue SQ-132  (=TRAC 8781 on olpc tracker), 

=== 2223hideHaloAfterDup-kfr.cs ===
Change Set:		hideHaloAfterDup-kfr
Date:			10 May 2009
Author:			Karl Ramberg

A dropped SelectionMorph still showed its halo, which could give nasty errors if a handle was clicked

=== 2222InhibitBlinkparen-yo.cs ===
Change Set:		InhibitBlinkparen-yo
Date:			16 April 2009
Author:			Yoshiki Ohshima

When preference blinkParen is false, it doesn't try to highlight  the matching opening paren for a type in.

=== 2221paintBoxLocation-kfr.cs ===
Change Set:		paintBoxLocation-kfr
Date:			10 May 2009
Author:			Karl Ramberg

Do some adjustment so the Paint pallette does not open slightly off screen when using Preferences unlimitedPaintArea and useBiggerPaintingBox

=== 2220squeaklandDevImage-yo.cs ===
ChangeSet:		squeaklandDevImage-yo
Date:			26 May 2009
Author:			Yoshiki Oshima

Supplies a method which, when evaluated, will transform the user's dev image into one suitable for ongoing Squeakland development work.

=== 2219categoryFullyVisible-sw.cs ===
Change Set:		categoryFullyVisible-sw
Date:			18 May 2009
Author:			Scott Wallace

When the user adds a new category-viewer to a viewer using the + button at the top of the viewer, make certain that all the contents of the category are fully visible, even that requires deleting another category viewer above the one in question.

Similarly, when the user retargets a category viewer to show a new category, make certain that the newly-chosen category is fully visible, deleting a category-viewer above the one in question if necessary.

=== 2218splitGeometry-sw.cs ===
Change Set:		splitGeometry-sw
Date:			18 May 2009
Author:			Scott Wallace

Splits the items formerly in the 'geometry' pane into two different categories -- for starters, the extra category is called 'more geometry', and the less widely-used items have been moved out to that new category.

Caution: several misspellings in help-string wordings are corrected here:


OLD:
The x coordiante of rotation center in parent's coodinate system.
The y coordiante of rotation center in parent

NEW:
The x coordinate of rotation center in parent's coordinate system
The y coordinate of rotation center in parent's coordinate system



=== 2217sdecimalRemove-yo.cs ===
Change Set:		sdecimalRemove-yo
Date:			22 May 2009
Author:			Yoshiki Ohshima

Remove the Test class for ScaledDecimal, which is holding onto an obsolete instance.

=== 2216ReadMona-yo.cs ===

=== 2215fixImmWin32-ka.cs ===
Change Set:		fixImmWin32-ka
Date:			7 April 2009
Author:			Kazuhiro Abe

setting correct CompositionWindowManager for HandMorph at startUp.
correcting prefereredKeyboardPosition by scale and offset of virtual screen.


=== 2214tabLoopFix-sw.cs ===
Change Set:		tabLoopFix-sw
Date:			2 April 2009
Author:			Scott Wallace

Fix to the bug that hitting tab key in a text field when the 'tabAmongFields' preference is false can result in an infinite loop.

=== 2213fixBlackScreen-yo.cs ===
Change Set:		fixBlackScreen-yo
Date:			4 April 2009
Author:			Yoshiki Ohshima

Avoid black screen flashing when switching same-sized project with display scale enabled for both. 

=== 2212haloClickTweak-sw.cs ===
Change Set:		haloClickTweak-sw
Date:			24 March 2009
Author:			Scott Wallace

JIRA SQ-143:  Make it less easy to rip a BookMorph's nav-bar out of the book.

This is another take on a fix.  This time, without removing the ability of a user to get a halo on any morph, the favorable properties of the existing algorithm (in regard to a a morph on a page of a BookMorph for example) are preserved while at the same time assuring that a first halo-click on the nav-bar of a book will bring up the halo for the entire book rather than for the nav-bar.

So I propose this as a replacement for the earlier workaround posted to SQ-143.

=== 2211renameFix-sw.cs ===
Change Set:		renameFix-sw
Date:			26 February 2009
Author:			Scott Wallace

Fix for TRAC 9032 - A script saved as textual asks for author initials

=== 2210scriptCatFix-sw.cs ===
Change Set:		scriptCatFix-sw
Date:			2 March 2009
Author:			Scott Wallace

Restore proper classification of scripts compiled from an etoy scriptor, that got lost a couple of years ago when 1305ScriptorToParseNode1-yo came into the etoys image.

=== 2209GStreamerUI18ForEtoys.cs ===
Change Set:		GStreamerUI18ForEtoys
Date:			26 March 2009
Author:			John McIntosh and others

The diff from the code in Etoys and GStreamer-UI-JMM.18.mcz

=== 2208GStreamerBase40ForEtoys.cs ===
Change Set:		GStreamerBase40ForEtoys
Date:			26 March 2009
Author:			John McIntosh and others

The diff from the code in Etoys and GStreamer-Base-JMM.40.mcz

=== 2207keepTempNames-yo.cs ===
Change Set:		keepTempNames-yo
Date:			18 March 2009
Author:			Yoshiki Ohshima

Keep temp names for tetually coded scripts.

=== 2206positionFixes-sw.cs ===
Change Set:		positionFixes-sw
Date:			4 February 2009
Author:			Scott Wallace

Fix for TRAC #9115: Etoys: 'bearing to' and 'distance to' tiles is calculated based on the top-
left corner. -- should be based on reference positions.

=== 2205virtDisplayJan12-yo.cs ===
Change Set:		virtDisplayJan12-yo
Date:			12 January 2009
Author:			Yoshiki Ohshima

Enable it a bit more eagerly.

=== 2204copyright-bf.cs ===
Change Set:		copyright-bf
Date:			8 January 2009
Author:			Bert Freudenberg

Put a full copyright notice into the image, make  Smalltalk>>copyright match that, and add a check to release builder so they remain consistent. Also, the about menu offers to show the license now.

=== 2203jaInputDec8-yo.cs ===
Change Set:		jaInputDec8-yo
Date:			8 December 2008
Author:			Yoshiki Ohshima

Make Anthy based Japanese input work.

=== 2202fullAuthTools-sw.cs ===
Change Set:		fullAuthTools-sw
Date:			5 January 2009
Author:			Scott Wallace

TRAC 9173:  Offer full authoring-tools menu to all users.
Show the same, full, set of choices in the authoring-tools menu whether or not eToyFriendly is turned on.

=== 2201FixOnSugarSandBox-yo.cs ===
Change Set:		FixnOnSugarSandBox-yo
Date:			15 December 2008
Author:			Yoshiki Ohshima

The logic was wrong when to enable the sandbox.

=== 2200dismissProjView-sw.cs ===
Change Set:		dismissProjView-sw
Date:			8 December 2008
Author:			Scott Wallace

Fix for TRAC 9058.  Cannot delete a ProjectViewMorph that represents a remote project.

=== 2199altDot-sw.cs ===
Change Set:		altDot-sw
Date:			23 November 2008
Author:			Scott Wallace

TRAC 8879 -- make alt-dot pause all ticking scripts even when eToyFriendly flag is off

=== 2198unCamelCase-sw.cs ===
Change Set:		unCamelCase-sw
Date:			3 November 2008
Author:			Scott Wallace

Fix for TRAC ticket 8929:
Should not attempt to 'un-camel-case' user-defined script and variable names.

User-defined script names and user-defined variable names will now always appear in the same form  accepted when the names are defined, which means they will always
 -- start with a lower-case alphabetic character
 -- consist entirely of alphabetic and numeric characters (NB:  no spaces)
Except for forcing the first character to be lower-case-alphabetic, the case of the identifiers dfor user-defined variable and script names will be exactly as the user typed them in.

These rules had gotten somewhat compromised with stamp-out-camel-case code earlier this year, but now are restored.  Note that *system-defined* method and variable names still undergo 'un-camel-casing', so that for example 'scaleFactor' appears on tiles as 'scale factor'.

=== 2197arrowsOnLangChg-sw.cs ===
Change Set:		arrowsOnLangChg-sw
Date:			3 November 2008
Author:			Scott Wallace

Fix for TRAC 8928 - Switching language loses arrows.

=== 2196wrap-swyo.cs ===
Change Set:		wrap-swyo
Date:			5 November 2008
Author:			Scott Wallace and Yoshiki Ohshima

A workaround for wrap issues in UserText and PreferencesPanel.

=== 2195koEnvironment.cs ===

=== 2194scriptNameTileFixup-sw.cs ===
Change Set:		scriptNameTileFixup-sw
Date:			22 October 2008
Author:			Scott Wallace

Some old projects from earlier versions of etoys can have faulty structure in ScriptNameTiles.  This update adds fixing up such tiles to the procedures undertaken under 'attempt misc repairs' in authoring-tools menu. 

=== 2193newDatastore-bf.cs ===
Change Set:		newDatastore-bf
Date:			28 October 2008
Author:			Bert Freudenberg

Cope with new Datastore using ByteArrays for Strings
